# Grain, a new functional language that compiles to wasm

https://github.com/grain-lang/grain
https://grain-lang.org/

## Create a hello.gr file

```grain
import Process from "sys/process"

print("🖐️ Hello, world! 🌍")


match (Process.argv()) {
  Ok(value) => print(value),
  Err(err) => fail "😡 ouch"
}
```

## Build hello

```bash
grain compile hello.gr 
```

## Run hello

```bash
grain run hello.gr.wasm
grain run hello.gr.wasm bob morane
```

## Run hello with wasmer

```bash
wasmer hello.gr.wasm bob morane
```

