#!/bin/bash
GOOS=js GOARCH=wasm go build -o hello.wasm
ls -lh *.wasm

# https://github.com/golang/go/issues/31105