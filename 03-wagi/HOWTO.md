# Wagi: WebAssembly Gateway Interface

_WAGI is the easiest way to get started writing WebAssembly microservices and web apps._

- https://github.com/deislabs/wagi

## Install Wagi

1. Get the latest binary release:
  ```
  wget https://github.com/deislabs/wagi/releases/download/v0.4.0/wagi-v0.4.0-linux-amd64.tar.gz
  ```
2. Unpack it `tar -zxf wagi-v0.4.0-linux-amd64.tar.gz`
3. Run the `./wagi --help` command

## Grain module

Add a `hello.gr` file in the `/modules` directory:

```grain
// hello.gr

import Process from "sys/process"

print("content-type: text/plain\n")

print("--------- ARGV ---------")
match (Process.argv()) {
  Ok(values) => {
    print(values)
  },
  Err(err) => fail "😡 ouch"
}


print("--------- ENV ---------")

match (Process.env()) {
  Ok(values) => {
    print(values)
  },
  Err(err) => fail "😡 ouch"
}
```

## Build the grain file

```bash
cd modules
grain compile hello.gr 
```

## Add a route

At the route of the project directory `03-wagi`, add a `modules.toml` file:

```toml
[[module]]
route = "/hello-grain"
module = "./modules/hello.gr.wasm"
```

## Run
Run:

```bash
./wagi -c ./modules.toml
```

## Test it

```bash
http http://localhost:3000/hello-grain
http http://localhost:3000/hello-grain?message=hello
curl $(gp url 3000)/hello-grain
curl $(gp url 3000)/hello-grain?message=hello
```
